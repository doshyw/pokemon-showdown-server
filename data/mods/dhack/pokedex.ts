export const Pokedex: {[k: string]: ModdedSpeciesData} = {
	bulbasaur: {
		inherit: true,
		maleOnlyHidden: true,
	},
	ivysaur: {
		inherit: true,
		maleOnlyHidden: true,
	},
	venusaur: {
		inherit: true,
		maleOnlyHidden: true,
	},
	charmander: {
		inherit: true,
		maleOnlyHidden: true,
	},
	charmeleon: {
		inherit: true,
		maleOnlyHidden: true,
	},
	charizard: {
		inherit: true,
		maleOnlyHidden: true,
	},
	squirtle: {
		inherit: true,
		maleOnlyHidden: true,
	},
	wartortle: {
		inherit: true,
		maleOnlyHidden: true,
	},
	blastoise: {
		inherit: true,
		maleOnlyHidden: true,
	},
	metapod: {
		inherit: true,
		baseStats: {hp: 40, atk: 20, def: 145, spa: 25, spd: 25, spe: 30},
		abilities: {0: "Shed Skin", 1: "Shell Armor"},
	},
	butterfree: {
		inherit: true,
		baseStats: {hp: 60, atk: 30, def: 80, spa: 115, spd: 95, spe: 95},
		abilities: {0: "Compound Eyes", 1: "Tinted Lens"},		
	},
	kakuna: {
		inherit: true,
		baseStats: {hp: 35, atk: 20, def: 140, spa: 25, spd: 25, spe: 35},
		abilities: {0: "Shed Skin", 1: "Shell Armor"},
	},
	beedrill: {
		inherit: true,
		baseStats: {hp: 70, atk: 105, def: 70, spa: 45, spd: 80, spe: 105},
		abilities: {0: "Sniper", 1: "Adaptability"},
	},
	pidgeot: {
		inherit: true,
		baseStats: {hp: 90, atk: 90, def: 85, spa: 70, spd: 85, spe: 111},
		abilities: {0: "Speed Boost", 1: "Tangled Feet"},
	},
	pikachu: {
		inherit: true,
		baseStats: {hp: 35, atk: 55, def: 30, spa: 50, spd: 40, spe: 90},
	},
	raichu: {
		inherit: true,
		baseStats: {hp: 60, atk: 90, def: 55, spa: 90, spd: 80, spe: 100},
	},
	nidoqueen: {
		inherit: true,
		baseStats: {hp: 90, atk: 82, def: 87, spa: 75, spd: 85, spe: 76},
	},
	nidoking: {
		inherit: true,
		baseStats: {hp: 81, atk: 92, def: 77, spa: 85, spd: 75, spe: 85},
	},
	clefairy: {
		inherit: true,
		types: ["Normal"],
	},
	clefable: {
		inherit: true,
		types: ["Normal"],
		baseStats: {hp: 95, atk: 70, def: 73, spa: 85, spd: 90, spe: 60},
	},
	jigglypuff: {
		inherit: true,
		types: ["Normal"],
		abilities: {0: "Cute Charm", H: "Friend Guard"},
	},
	wigglytuff: {
		inherit: true,
		types: ["Normal"],
		baseStats: {hp: 140, atk: 70, def: 45, spa: 75, spd: 50, spe: 45},
		abilities: {0: "Cute Charm", H: "Frisk"},
	},
	vileplume: {
		inherit: true,
		baseStats: {hp: 90, atk: 70, def: 95, spa: 120, spd: 95, spe: 50},
		abilities: {0: "Effect Spore", 1: "Stench"},
	},
	poliwrath: {
		inherit: true,
		baseStats: {hp: 90, atk: 85, def: 95, spa: 70, spd: 90, spe: 70},
	},
	alakazam: {
		inherit: true,
		baseStats: {hp: 55, atk: 50, def: 45, spa: 135, spd: 85, spe: 120},
	},
	victreebel: {
		inherit: true,
		baseStats: {hp: 80, atk: 105, def: 65, spa: 100, spd: 60, spe: 70},
	},
	golem: {
		inherit: true,
		baseStats: {hp: 80, atk: 110, def: 130, spa: 55, spd: 65, spe: 45},
	},
	krabby: {
		inherit: true,
		baseStats: {hp: 45, atk: 105, def: 90, spa: 25, spd: 45, spe: 50},
			abilities: {0: "Sheer Force", 1: "Shell Armor"},
	},
	kingler: {
	inherit: true,
	baseStats: {hp: 70, atk: 130, def: 115, spa: 50, spd: 70, spe: 80},
	abilities: {0: "Sheer Force", 1: "Shell Armor"},
	},
	mrmime: {
		inherit: true,
		types: ["Psychic"],
	},
	articuno: {
		inherit: true,
		unreleasedHidden: true,
	},
	zapdos: {
		inherit: true,
		abilities: {0: "Pressure", H: "Lightning Rod"},
		unreleasedHidden: true,
	},
	moltres: {
		inherit: true,
		unreleasedHidden: true,
	},
	chikorita: {
		inherit: true,
		unreleasedHidden: true,
	},
	bayleef: {
		inherit: true,
		unreleasedHidden: true,
	},
	meganium: {
		inherit: true,
		unreleasedHidden: true,
	},
	cyndaquil: {
		inherit: true,
		unreleasedHidden: true,
	},
	quilava: {
		inherit: true,
		unreleasedHidden: true,
	},
	typhlosion: {
		inherit: true,
		unreleasedHidden: true,
	},
	totodile: {
		inherit: true,
		unreleasedHidden: true,
	},
	croconaw: {
		inherit: true,
		unreleasedHidden: true,
	},
	feraligatr: {
		inherit: true,
		unreleasedHidden: true,
	},
	igglybuff: {
		inherit: true,
		types: ["Normal"],
		abilities: {0: "Cute Charm", H: "Friend Guard"},
	},
	togepi: {
		inherit: true,
		types: ["Normal"],
	},
	togetic: {
		inherit: true,
		types: ["Normal", "Flying"],
	},
	cleffa: {
		inherit: true,
		types: ["Normal"],
	},
	ampharos: {
		inherit: true,
		baseStats: {hp: 90, atk: 75, def: 75, spa: 115, spd: 90, spe: 55},
	},
	bellossom: {
		inherit: true,
		baseStats: {hp: 70, atk: 120, def: 80, spa: 50, spd: 80, spe: 110},
		abilities: {0: "Natural Cure", 1: "Serene Grace"}
	},
	marill: {
		inherit: true,
		types: ["Water"],
	},
	azumarill: {
		inherit: true,
		types: ["Water"],
		baseStats: {hp: 100, atk: 50, def: 80, spa: 50, spd: 80, spe: 50},
	},
	jumpluff: {
		inherit: true,
		baseStats: {hp: 75, atk: 55, def: 70, spa: 55, spd: 85, spe: 110},
	},
	snubbull: {
		inherit: true,
		types: ["Fairy"],
		baseStats: {hp: 60, atk: 80, def: 50, spa: 40, spd: 50, spe: 30},
		abilities: {0: "Intimidate", 1: "Rattled"}
	},
	granbull: {
		inherit: true,
		types: ["Fairy"],
		baseStats: {hp: 90, atk: 120, def: 85, spa: 60, spd: 80, spe: 65},
		abilities: {0: "Intimidate", 1: "Rattled"}
	},
	tyrogue: {
		inherit: true,
		maleOnlyHidden: true,
	},
	hitmonlee: {
		inherit: true,
		maleOnlyHidden: true,
	},
	hitmonchan: {
		inherit: true,
		maleOnlyHidden: true,
	},
	hitmontop: {
		inherit: true,
		maleOnlyHidden: true,
	},
	treecko: {
		inherit: true,
		maleOnlyHidden: true,
	},
	grovyle: {
		inherit: true,
		maleOnlyHidden: true,
	},
	sceptile: {
		inherit: true,
		maleOnlyHidden: true,
	},
	torchic: {
		inherit: true,
		maleOnlyHidden: true,
	},
	combusken: {
		inherit: true,
		maleOnlyHidden: true,
	},
	blaziken: {
		inherit: true,
		maleOnlyHidden: true,
	},
	mudkip: {
		inherit: true,
		maleOnlyHidden: true,
	},
	marshtomp: {
		inherit: true,
		maleOnlyHidden: true,
	},
	swampert: {
		inherit: true,
		maleOnlyHidden: true,
	},
	beautifly: {
		inherit: true,
		baseStats: {hp: 70, atk: 50, def: 50, spa: 110, spd: 50, spe: 120},
		abilities: {0: "Shield Dust", 1: "Wonder Skin"},
	},
	lotad: {
		inherit: true,
		baseStats: {hp: 50, atk: 30, def: 30, spa: 50, spd: 50, spe: 40},
		abilities: {0: "Unaware", 1: "Own Tempo"},
	},
	lombre: {
		inherit: true,
		abilities: {0: "Unaware", 1: "Own Tempo"},
	},
	ludicolo: {
		inherit: true,
		baseStats: {hp: 80, atk: 70, def: 70, spa: 100, spd: 100, spe: 80},
		abilities: {0: "Unaware", 1: "Own Tempo"},
	},
	ralts: {
		inherit: true,
		types: ["Psychic"],
	},
	kirlia: {
		inherit: true,
		types: ["Psychic"],
	},
	gardevoir: {
		inherit: true,
		types: ["Psychic"],
	},
	exploud: {
		inherit: true,
		baseStats: {hp: 104, atk: 91, def: 63, spa: 91, spd: 63, spe: 68},
	},
	azurill: {
		inherit: true,
		types: ["Normal"],
	},
	mawile: {
		inherit: true,
		types: ["Steel"],
	},
	plusle: {
		inherit: true,
		abilities: {0: "Plus"},
	},
	minun: {
		inherit: true,
		abilities: {0: "Minus"},
	},
	feebas: {
		inherit: true,
		abilities: {0: "Oblivious", 1: "Clear Body"},
	},
	milotic: {
		inherit: true,
		abilities: {0: "Cute Charm", 1: "Marvel Scale"},
		baseStats: {hp: 95, atk: 60, def: 90, spa: 100, spd: 125, spe: 80},
	},
	duskull: {
		inherit: true,
		abilities: {0: "Levitate"},
	},
	dusclops: {
		inherit: true,
		abilities: {0: "Pressure"},
	},
	regirock: {
		inherit: true,
		unreleasedHidden: true,
	},
	regice: {
		inherit: true,
		unreleasedHidden: true,
	},
	registeel: {
		inherit: true,
		unreleasedHidden: true,
	},
	turtwig: {
		inherit: true,
		maleOnlyHidden: true,
	},
	grotle: {
		inherit: true,
		maleOnlyHidden: true,
	},
	torterra: {
		inherit: true,
		maleOnlyHidden: true,
	},
	chimchar: {
		inherit: true,
		maleOnlyHidden: true,
	},
	monferno: {
		inherit: true,
		maleOnlyHidden: true,
	},
	infernape: {
		inherit: true,
		maleOnlyHidden: true,
	},
	piplup: {
		inherit: true,
		maleOnlyHidden: true,
	},
	prinplup: {
		inherit: true,
		maleOnlyHidden: true,
	},
	empoleon: {
		inherit: true,
		maleOnlyHidden: true,
	},
	starly: {
		inherit: true,
		abilities: {0: "Keen Eye", 1: "Reckless"},
	},
	staravia: {
		inherit: true,
		abilities: {0: "Keen Eye", 1: "Reckless"}
	},		
	staraptor: {
		inherit: true,
		baseStats: {hp: 95, atk: 120, def: 80, spa: 50, spd: 60, spe: 100},
		abilities: {0: "Keen Eye", 1: "Reckless"}
	},
	roserade: {
		inherit: true,
		baseStats: {hp: 60, atk: 70, def: 55, spa: 125, spd: 105, spe: 90},
	},
	mimejr: {
		inherit: true,
		types: ["Psychic"],
	},
	togekiss: {
		inherit: true,
		types: ["Normal", "Flying"],
	},
	dusknoir: {
		inherit: true,
		abilities: {0: "Pressure"},
	},
	snivy: {
		inherit: true,
		unreleasedHidden: true,
		abilities: {0: "Overgrow", 1: "Contrary"}
	},
	servine: {
		inherit: true,
		unreleasedHidden: true,
		abilities: {0: "Overgrow", 1: "Contrary"}
	},
	serperior: {
		inherit: true,
		unreleasedHidden: true,
		baseStats: {hp: 75, atk: 65, def: 95, spa: 85, spd: 95, spe: 115},
		abilities: {0: "Overgrow", 1: "Contrary"}
	},
	tepig: {
		inherit: true,
		unreleasedHidden: true,
		abilities: {0: "Blaze", 1: "Sheer Force"}
	},
	pignite: {
		inherit: true,
		unreleasedHidden: true,
		abilities: {0: "Blaze", 1: "Sheer Force"}
	},
	emboar: {
		inherit: true,
		unreleasedHidden: true,
		baseStats: {hp: 110, atk: 125, def: 75, spa: 90, spd: 65, spe: 65},
		abilities: {0: "Blaze", 1: "Sheer Force"}
	},
	oshawott: {
		inherit: true,
		unreleasedHidden: true,
		baseStats: {hp: 45, atk: 65, def: 45, spa: 63, spd: 45, spe: 45},
		abilities: {0: "Torrent", 1: "Battle Armor"}
	},
	dewott: {
		inherit: true,
		unreleasedHidden: true,
		baseStats: {hp: 65, atk: 85, def: 60, spa: 83, spd: 60, spe: 60},
		abilities: {0: "Torrent", 1: "Battle Armor"}
	},
	samurott: {
		inherit: true,
		unreleasedHidden: true,
		baseStats: {hp: 85, atk: 110, def: 85, spa: 110, spd: 70, spe: 70},
		abilities: {0: "Torrent", 1: "Battle Armor"}
	},
	stoutland: {
		inherit: true,
		baseStats: {hp: 85, atk: 110, def: 90, spa: 45, spd: 100, spe: 80},
		abilities: {0: "Illuminate", 1: "Defiant"}
	},
	unfezant: {
		inherit: true,
		baseStats: {hp: 80, atk: 115, def: 80, spa: 70, spd: 90, spe: 93},
		abilities: {0: "Sniper", 1: "Super Luck"}
	},
	gigalith: {
		inherit: true,
		baseStats: {hp: 85, atk: 135, def: 130, spa: 60, spd: 70, spe: 25},
	},
	seismitoad: {
		inherit: true,
		baseStats: {hp: 105, atk: 85, def: 75, spa: 85, spd: 75, spe: 74},
	},
	venipede: {
		inherit: true,
		abilities: {0: "Poison Point", 1: "Swarm", H: "Quick Feet"},
	},
	whirlipede: {
		inherit: true,
		abilities: {0: "Poison Point", 1: "Swarm", H: "Quick Feet"},
	},
	scolipede: {
		inherit: true,
		baseStats: {hp: 60, atk: 90, def: 89, spa: 55, spd: 69, spe: 112},
		abilities: {0: "Poison Point", 1: "Swarm", H: "Quick Feet"},
	},
	cottonee: {
		inherit: true,
		types: ["Grass"],
	},
	whimsicott: {
		inherit: true,
		types: ["Grass"],
	},
	basculinbluestriped: {
		inherit: true,
		abilities: {0: "Rock Head", 1: "Adaptability", H: "Mold Breaker", S: "Reckless"},
	},
	krookodile: {
		inherit: true,
		baseStats: {hp: 95, atk: 117, def: 70, spa: 65, spd: 70, spe: 92},
	},
	gothita: {
		inherit: true,
		unreleasedHidden: true,
	},
	gothorita: {
		inherit: true,
		abilities: {0: "Frisk", H: "Shadow Tag"},
		maleOnlyHidden: true,
	},
	gothitelle: {
		inherit: true,
		abilities: {0: "Frisk", H: "Shadow Tag"},
		maleOnlyHidden: true,
	},
	ferrothorn: {
		inherit: true,
		abilities: {0: "Iron Barbs"},
	},
	klink: {
		inherit: true,
		unreleasedHidden: true,
	},
	litwick: {
		inherit: true,
		abilities: {0: "Flash Fire", 1: "Flame Body", H: "Shadow Tag"},
		unreleasedHidden: true,
	},
	lampent: {
		inherit: true,
		abilities: {0: "Flash Fire", 1: "Flame Body", H: "Shadow Tag"},
		unreleasedHidden: true,
	},
	chandelure: {
		inherit: true,
		abilities: {0: "Flash Fire", 1: "Flame Body", H: "Shadow Tag"},
		unreleasedHidden: true,
	},
	rufflet: {
		inherit: true,
		unreleasedHidden: true,
	},
	larvesta: {
		inherit: true,
		unreleasedHidden: true,
	},
	volcarona: {
		inherit: true,
		unreleasedHidden: true,
	},
	wooper: {
		inherit: true,
		baseStats: {hp: 75, atk: 65, def: 55, spa: 25, spd: 55, spe: 15},
		abilities: {0: "Water Absorb", 1: "Unaware"},
	},
	quagsire: {
		inherit: true,
		baseStats: {hp: 115, atk: 95, def: 95, spa: 65, spd: 85, spe: 35},
		abilities: {0: "Water Absorb", 1: "Unaware"},
	},
	magikarp: {
		inherit: true,
		abilities: {0: "Swift Swim", 1: "Rattled"},
	},		
	gyarados: {
		inherit: true,
		baseStats: {hp: 95, atk: 125, def: 80, spa: 60, spd: 100, spe: 90},
		abilities: {0: "Intimidate", 1: "Moxie"},
	},		
	spinarak: {
		inherit: true,
		baseStats: {hp: 40, atk: 70, def: 60, spa: 40, spd: 60, spe: 50}
	},			
	ariados: {
		inherit: true,
		baseStats: {hp: 90, atk: 90, def: 80, spa: 60, spd: 80, spe: 70},
		abilities: {0: "Swarm", 1: "Sniper"},
	},
	sentret: {
		inherit: true,
		baseStats: {hp: 40, atk: 50, def: 35, spa: 30, spd: 45, spe: 50},
		abilities: {0: "Frisk", 1: "Run Away"},
	},		
	furret: {
		inherit: true,
		baseStats: {hp: 95, atk: 90, def: 65, spa: 45, spd: 65, spe: 110},
		abilities: {0: "Frisk", 1: "Technician"},
	},			
	wurmple: {
		inherit: true,
		abilities: {0: "Shield Dust", 1: "Run Away"},
	},		
	silcoon: {
		inherit: true,
	},				
	dustox: {
		inherit: true,
		baseStats: {hp: 70, atk: 50, def: 80, spa: 80, spd: 100, spe: 70},
		abilities: {0: "Shield Dust", 1: "Compound Eyes"},
	},		
	oddish: {
		inherit: true,
		abilities: {0: "Natural Cure", 1: "Run Away"},
	}, 
	gloom: {
		inherit: true,
		abilities: {0: "Effect Spore", 1: "Stench"},
	},		
	mankey: {
		inherit: true,
		baseStats: {hp: 45, atk: 80, def: 45, spa: 35, spd: 45, spe: 70},
		abilities: {0: "Defiant", 1: "Anger Point"},
	},		
	primeape: {
		inherit: true,
		baseStats: {hp: 95, atk: 105, def: 70, spa: 60, spd: 70, spe: 101},
		abilities: {0: "Defiant", 1: "Anger Point"},
	},		
	ledyba: {
		inherit: true,
		baseStats: {hp: 40, atk: 60, def: 30, spa: 20, spd: 80, spe: 70},
		abilities: {0: "Iron Fist", 1: "Rattled"},
	},		
	ledian: {
		inherit: true,
		baseStats: {hp: 60, atk: 90, def: 70, spa: 50, spd: 110, spe: 110},
		abilities: {0: "Iron Fist", 1: "Technician"},
	},		
	audino: {
		inherit: true,
		types: ["Fairy"],
		baseStats: {hp: 110, atk: 60, def: 90, spa: 80, spd: 90, spe: 50},
		abilities: {0: "Regenerator", 1: "Regenerator"},
	},		
	aipom: {
		inherit: true,
	},		
	ambipom: {
		inherit: true,
		baseStats: {hp: 90, atk: 100, def: 80, spa: 60, spd: 70, spe: 120},
		abilities: {0: "Technician", 1: "Skill Link"},
	},		
	surskit: {
		inherit: true,
		baseStats: {hp: 40, atk: 30, def: 32, spa: 60, spd: 52, spe: 65}
	},		
	masquerain: {
		inherit: true,
		baseStats: {hp: 70, atk: 60, def: 62, spa: 100, spd: 82, spe: 90},
		abilities: {0: "Intimidate", 1: "Intimidate"},
	},		
	weedle: {
		inherit: true,
		abilities: {0: "Shield Dust", 1: "Run Away"},
	},		
	sewaddle: {
		inherit: true,
		abilities: {0: "Swarm", 1: "Shed Skin"},
	},	
	swadloon: {
		inherit: true,
		abilities: {0: "Swarm", 1: "Shed Skin"},
	},		
	leavanny: {
		inherit: true,
		baseStats: {hp: 75, atk: 125, def: 80, spa: 30, spd: 70, spe: 125}
	},	
	pidgey: {
		inherit: true,
		baseStats: {hp: 40, atk: 55, def: 50, spa: 25, spd: 45, spe: 61},
		abilities: {0: "Big Pecks", 1: "Tangled Feet"},
	},	
	pidgeotto: {
		inherit: true,
		baseStats: {hp: 63, atk: 70, def: 65, spa: 40, spd: 60, spe: 71},
		abilities: {0: "Big Pecks", 1: "Tangled Feet"},
	},	
	spearow: {
		inherit: true,
		baseStats: {hp: 40, atk: 65, def: 35, spa: 30, spd: 35, spe: 70},
		abilities: {0: "Keen Eye", 1: "Sniper"},
	},
	fearow: {
		inherit: true,
		baseStats: {hp: 75, atk: 95, def: 70, spa: 60, spd: 70, spe: 101},
		abilities: {0: "Intimidate", 1: "Sniper"},
	},
	herdier: {
		inherit: true,
		abilities: {0: "Intimidate", 1: "Defiant"},
	},
	poochyena: {
		inherit: true,
		baseStats: {hp: 55, atk: 65, def: 45, spa: 20, spd: 40, spe: 50},
		abilities: {0: "Run Away", 1: "Rattled"},
	},
	mightyena: {
		inherit: true,
		baseStats: {hp: 80, atk: 100, def: 70, spa: 60, spd: 80, spe: 90},
		abilities: {0: "Intimidate", 1: "Moxie"},
	},	
	houndour: {
		inherit: true,
		baseStats: {hp: 45, atk: 40, def: 50, spa: 80, spd: 50, spe: 75},
		abilities: {0: "Unnerve", 1: "Flash Fire"},
	},		
	houndoom: {
		inherit: true,
		baseStats: {hp: 75, atk: 70, def: 60, spa: 110, spd: 90, spe: 105},
		abilities: {0: "Intimidate", 1: "Flash Fire"},
	},
	growlithe: {
		inherit: true,
		baseStats: {hp: 60, atk: 70, def: 50, spa: 60, spd: 50, spe: 60},
		abilities: {0: "Intimidate", 1: "Justified"},
	},
	arcanine: {
		inherit: true,
		baseStats: {hp: 90, atk: 110, def: 80, spa: 80, spd: 80, spe: 115},
		abilities: {0: "Intimidate", 1: "Justified"},
	},
	riolu: {
		inherit: true,
		abilities: {0: "Steadfast", 1: "Prankster"},
	},
	lucario: {
		inherit: true,
		baseStats: {hp: 80, atk: 120, def: 70, spa: 80, spd: 70, spe: 100},
		abilities: {0: "Adaptability", 1: "Justified"},
	},
	dunsparce: {
		inherit: true,
		types: ["Normal", "Dragon"],
		baseStats: {hp: 110, atk: 70, def: 90, spa: 70, spd: 85, spe: 45},
		abilities: {0: "Serene Grace"},
	},
	minccino: {
		inherit: true,
		baseStats: {hp: 60, atk: 60, def: 50, spa: 40, spd: 50, spe: 90},
		abilities: {0: "Technician", 1: "Skill Link"},
	},
	cinccino: {
		inherit: true,
		baseStats: {hp: 90, atk: 100, def: 60, spa: 65, spd: 60, spe: 115},
		abilities: {0: "Technician", 1: "Skill Link"},
	},
	rattata: {
		inherit: true,
		baseStats: {hp: 30, atk: 56, def: 35, spa: 25, spd: 35, spe: 82}
	},
	raticate: {
		inherit: true,
		baseStats: {hp: 75, atk: 82, def: 60, spa: 50, spd: 70, spe: 117},
		abilities: {0: "Hustle", 1: "Guts"},
	},
	miltank: {
		inherit: true,
		abilities: {0: "Thick Fat", 1: "Sap Sipper"},
	},
	kecleon: {
		inherit: true,
		baseStats: {hp: 80, atk: 90, def: 70, spa: 60, spd: 120, spe: 40}
	},
	zigzagoon: {
		inherit: true,
		baseStats: {hp: 50, atk: 50, def: 40, spa: 30, spd: 40, spe: 70},
		abilities: {0: "Pickup", 1: "Quick Feet"}
	},
	linoone: {
		inherit: true,
		baseStats: {hp: 90, atk: 80, def: 60, spa: 50, spd: 60, spe: 120},
		abilities: {0: "Gluttony", 1: "Quick Feet"},
	},
	skitty: {
		inherit: true,
		types: ["Normal", "Fairy"],
		baseStats: {hp: 80, atk: 80, def: 45, spa: 35, spd: 35, spe: 100},
		abilities: {0: "Serene Grace", 1: "Wonder Skin"},
	},
	delcatty: {
		inherit: true,
		types: ["Normal", "Fairy"],
		baseStats: {hp: 80, atk: 90, def: 65, spa: 90, spd: 55, spe: 100},
		abilities: {0: "Serene Grace", 1: "Wonder Skin"},
	},
	happiny: {
		inherit: true,
		baseStats: {hp: 100, atk: 5, def: 5, spa: 25, spd: 75, spe: 30},
		abilities: {0: "Natural Cure", 1: "Serene Grace"},
	},
	chansey: {
		inherit: true,
		baseStats: {hp: 250, atk: 5, def: 5, spa: 45, spd: 105, spe: 50}
	},
	blissey: {
		inherit: true,
	},
	pidove: {
		inherit: true,
		baseStats: {hp: 60, atk: 65, def: 50, spa: 35, spd: 55, spe: 45},
		abilities: {0: "Sniper", 1: "Super Luck"},
	},
	tranquill: {
		inherit: true,
		baseStats: {hp: 70, atk: 85, def: 60, spa: 50, spd: 60, spe: 65},
		abilities: {0: "Sniper", 1: "Super Luck"},
	},
	meowth: {
		inherit: true,
		baseStats: {hp: 50, atk: 35, def: 40, spa: 40, spd: 100, spe: 90}
	},
	persian: {
		inherit: true,
		baseStats: {hp: 75, atk: 80, def: 70, spa: 65, spd: 70, spe: 130}
	},	
	spinda: {
		inherit: true,
		baseStats: {hp: 80, atk: 80, def: 80, spa: 80, spd: 80, spe: 80},
		abilities: {0: "Own Tempo", 1: "Contrary"}
	},	
};
